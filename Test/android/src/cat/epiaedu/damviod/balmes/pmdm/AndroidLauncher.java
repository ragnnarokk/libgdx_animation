package cat.epiaedu.damviod.balmes.pmdm;

import android.os.Bundle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import cat.epiaedu.damviod.balmes.pmdm.JocTest;

public class AndroidLauncher extends AndroidApplication {
	@Override
	private SpriteBatch batch;
	private BitmapFont font;
	private float elapsedTime = 0;
	private TextureAtlas textureAtlas;
	private Animation animation;

	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new JocTest(), config);
	}
	public void create() {

		batch = new SpriteBatch();

		textureAtlas = new TextureAtlas(Gdx.files.internal("atlas/spritesheet.atlas"));

		animation = new Animation(1 / 15f, textureAtlas.getRegions());

	}

	@Override

	public void render() {

		Gdx.gl.glClearColor(0, 0, 0, 1);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		elapsedTime += Gdx.graphics.getDeltaTime();

		batch.draw((Texture) animation.getKeyFrame(elapsedTime, true), 0, 0);

		batch.end();

	}

	@Override

	public void dispose() {

		batch.dispose();

		textureAtlas.dispose();

	}
}
