package cat.epiaedu.damviod.balmes.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;

public class JocTest extends ApplicationAdapter implements GestureListener,InputProcessor {
	SpriteBatch batch;
	private float elapsedTime;
	TextureAtlas textureAtlas;
	Animation animation;
	float posX;
	float posY;
    boolean moveX;
    boolean moveY;
	Sound musicaMotor;
	float speed = 0;
    long musicM;
    float pitch =0;

    @Override
	public void create () {
		batch = new SpriteBatch();
		textureAtlas = new TextureAtlas(Gdx.files.internal("pack.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		musicaMotor = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
		InputMultiplexer im = new InputMultiplexer();
		GestureDetector gd = new GestureDetector(this);
		im.addProcessor(gd);
		im.addProcessor(this);
		Gdx.input.setInputProcessor(im);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(Gdx.input.isKeyPressed(Input.Keys.D)) {
            speed = speed + 1f;
            if(speed >=200) speed = 200;
            System.out.println(speed);
            pitch = 0.5f + speed/201 * 1f;
            System.out.println(pitch);
            musicaMotor.setLooping(musicM,true);
			musicaMotor.setPitch(musicM,pitch);
		}else{
            speed =0;
            pitch =0;
        }
        if(moveX)
        {
            posX+=50*Gdx.graphics.getDeltaTime();
        }
        else if(!moveX)
        {
            posX-=50*Gdx.graphics.getDeltaTime();

        }
        if(moveY)
        {
            posY+=50*Gdx.graphics.getDeltaTime();
        }
        else if(!moveY)
        {
            posY-=50*Gdx.graphics.getDeltaTime();
        }
		batch.begin();
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), posX, posY);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		textureAtlas.dispose();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == com.badlogic.gdx.Input.Keys.A)
		{
			moveX = false;
		}
			else if(keycode == com.badlogic.gdx.Input.Keys.D)
		{
			moveX = true;
            musicaMotor.play();
            musicM = musicaMotor.play();
		}
		if(keycode == com.badlogic.gdx.Input.Keys.W)
		{
			moveY = true;
		}
		else if(keycode == com.badlogic.gdx.Input.Keys.S)
		{
			moveY = false;
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == com.badlogic.gdx.Input.Keys.D)
		{
			musicaMotor.stop();
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
